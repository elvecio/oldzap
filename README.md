**ZAP**

Zap is a social networking app running under the Zot/6 protocol and the LAMP web stack.

Protocol documentation is located here:

https://macgirvin.com/wiki/mike/Zot%2BVI/Home

Zap is based on Red, which in turn is based on Hubzilla. It is otherwise unrelated to those projects and the software has a completely different scope and purpose. 







